package ru.manasyan.patterns.abstractfactory;

import ru.manasyan.patterns.abstractfactory.factory.BwmCar;
import ru.manasyan.patterns.abstractfactory.factory.AbstractCar;
import ru.manasyan.patterns.abstractfactory.factory.ToyotaCar;

public class Main {

    public static void main(String[] args) {

        AbstractCar bmwCar = new BwmCar("Model X5", "White", "Sedan", "300",
                "Electro", "Standard", "Brown");
        bmwCar.build();

        AbstractCar toyotaCar = new ToyotaCar("Corolla", "Green", "Hatchback", "120",
                "Diesel", "Standard", "Black");
        toyotaCar.build();

        print(bmwCar);
        print(toyotaCar);

    }

    private static void print(AbstractCar car) {
        System.out.println("===== Car Info ======");
        System.out.println(car.getModel());
        System.out.println(car.getBody().getName());
        System.out.println(car.getEngine().getName());
        System.out.println(car.getInterior().getName());
        System.out.println("---------------------");
    }


}
