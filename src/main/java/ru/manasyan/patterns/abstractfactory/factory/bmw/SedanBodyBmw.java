package ru.manasyan.patterns.abstractfactory.factory.bmw;

import ru.manasyan.patterns.abstractfactory.factory.IBody;

public class SedanBodyBmw implements IBody {

    private String name = "SedanBodyBmw";

    @Override
    public String getName() {
        return name;
    }
}
