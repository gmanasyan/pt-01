package ru.manasyan.patterns.abstractfactory.factory.bmw;

import ru.manasyan.patterns.abstractfactory.factory.IInterior;

public class PremiumInteriorBmw implements IInterior {

    private String name = "PremiumInteriorBmw";

    @Override
    public String getName() {
        return name;
    }

}
