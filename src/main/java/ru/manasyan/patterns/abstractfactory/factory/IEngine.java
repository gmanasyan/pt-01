package ru.manasyan.patterns.abstractfactory.factory;

public interface IEngine {
    String getName();
}
