package ru.manasyan.patterns.abstractfactory.factory.toyota;

import ru.manasyan.patterns.abstractfactory.factory.IInterior;

public class PremiumInteriorToyota implements IInterior {

    private String name = "PremiumInteriorToyota";

    @Override
    public String getName() {
        return name;
    }

}
