package ru.manasyan.patterns.abstractfactory.factory.toyota;

import ru.manasyan.patterns.abstractfactory.factory.IEngine;

public class BenzineEngineToyota implements IEngine {

    private String name = "BenzineEngineToyota";

    @Override
    public String getName() {
        return name;
    }
}
