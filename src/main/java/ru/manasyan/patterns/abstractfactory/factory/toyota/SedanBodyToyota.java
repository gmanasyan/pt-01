package ru.manasyan.patterns.abstractfactory.factory.toyota;

import ru.manasyan.patterns.abstractfactory.factory.IBody;

public class SedanBodyToyota implements IBody {

    private String name = "SedanBodyToyota";

    @Override
    public String getName() {
        return name;
    }

}
