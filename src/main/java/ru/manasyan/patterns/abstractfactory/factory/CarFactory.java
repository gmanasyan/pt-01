package ru.manasyan.patterns.abstractfactory.factory;

public interface CarFactory {

    IBody createBody(String body);

    IEngine createEngine(String engine);

    IInterior createInterior(String interior);


}
