package ru.manasyan.patterns.abstractfactory.factory;

import ru.manasyan.patterns.abstractfactory.factory.bmw.BwmFactory;

public class BwmCar extends AbstractCar {

    private String model;
    private String bodyType;
    private String engineType;
    private String interiorType;

    private IBody body;
    private IEngine engine;
    private IInterior interior;

    private final CarFactory factory = new BwmFactory();

    public BwmCar(String model, String color, String bodyType, String horsePower,
                  String engineType, String interiorType, String interiorColor) {
        this.model = model;
        this.bodyType = bodyType;
        this.engineType = engineType;
        this.interiorType = interiorType;
    }

    @Override
    public void build() {
        body = factory.createBody(bodyType);
        engine = factory.createEngine(engineType);
        interior = factory.createInterior(interiorType);
    }

    @Override
    public IBody getBody() {
        return body;
    }

    @Override
    public IEngine getEngine() {
        return engine;
    }

    @Override
    public IInterior getInterior() {
        return interior;
    }

    @Override
    public String getModel() {
        return model;
    }

    @Override
    public void setModel(String model) {
        this.model = model;
    }

    @Override
    public String getBodyType() {
        return bodyType;
    }

    @Override
    public void setBodyType(String bodyType) {
        this.bodyType = bodyType;
    }
}
