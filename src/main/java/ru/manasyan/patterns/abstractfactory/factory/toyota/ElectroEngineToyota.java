package ru.manasyan.patterns.abstractfactory.factory.toyota;

import ru.manasyan.patterns.abstractfactory.factory.IEngine;

public class ElectroEngineToyota implements IEngine {

    private String name = "ElectroEngineToyota";

    @Override
    public String getName() {
        return name;
    }

}
