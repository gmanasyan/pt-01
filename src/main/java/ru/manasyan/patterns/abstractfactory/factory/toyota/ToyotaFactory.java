package ru.manasyan.patterns.abstractfactory.factory.toyota;

import ru.manasyan.patterns.abstractfactory.factory.CarFactory;
import ru.manasyan.patterns.abstractfactory.factory.IBody;
import ru.manasyan.patterns.abstractfactory.factory.IEngine;
import ru.manasyan.patterns.abstractfactory.factory.IInterior;
import ru.manasyan.patterns.abstractfactory.factory.bmw.*;

public class ToyotaFactory implements CarFactory {

    @Override
    public IBody createBody(String body) {
        switch (body) {
            case "Sedan" :  return new SedanBodyToyota();
            case "Hatchback" :  return new HatchbackBodyToyota();
        }
        return new SedanBodyToyota();
    }

    @Override
    public IEngine createEngine(String engine) {
        switch (engine) {
            case "Benzine" :  return new BenzineEngineToyota();
            case "Electro" :  return new ElectroEngineToyota();
            case "Diesel" :  return new DieselEngineToyota();
        }
        return new BenzineEngineToyota();
    }

    @Override
    public IInterior createInterior(String interior) {
        switch (interior) {
            case "Standard" :  return new StandardInteriorToyota();
            case "Premium" :  return new PremiumInteriorToyota();
        }
        return new StandardInteriorToyota();
    }


}
