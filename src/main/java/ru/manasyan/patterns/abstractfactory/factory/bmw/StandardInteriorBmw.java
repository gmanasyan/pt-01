package ru.manasyan.patterns.abstractfactory.factory.bmw;

import ru.manasyan.patterns.abstractfactory.factory.IInterior;

public class StandardInteriorBmw implements IInterior {

    private String name = "StandardInteriorBmw";

    @Override
    public String getName() {
        return name;
    }

}
