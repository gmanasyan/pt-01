package ru.manasyan.patterns.abstractfactory.factory.toyota;

import ru.manasyan.patterns.abstractfactory.factory.IBody;

public class HatchbackBodyToyota implements IBody {

    private String name = "HatchbackBodyToyota";

    @Override
    public String getName() {
        return name;
    }

}
