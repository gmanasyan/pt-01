package ru.manasyan.patterns.abstractfactory.factory.bmw;

import ru.manasyan.patterns.abstractfactory.factory.IEngine;

public class DieselEngineBmw implements IEngine {

    private String name = "DieselEngineBmw";

    @Override
    public String getName() {
        return name;
    }

}
