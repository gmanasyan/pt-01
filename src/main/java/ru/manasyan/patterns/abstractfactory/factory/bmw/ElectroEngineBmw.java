package ru.manasyan.patterns.abstractfactory.factory.bmw;

import ru.manasyan.patterns.abstractfactory.factory.IEngine;

public class ElectroEngineBmw implements IEngine {

    private String name = "ElectroEngineBmw";

    @Override
    public String getName() {
        return name;
    }

}
