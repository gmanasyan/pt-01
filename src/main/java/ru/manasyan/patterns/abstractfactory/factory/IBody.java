package ru.manasyan.patterns.abstractfactory.factory;

public interface IBody {
    String getName();
}
