package ru.manasyan.patterns.abstractfactory.factory;

public abstract class AbstractCar {

    public abstract void build();

    public abstract IBody getBody();

    public abstract IEngine getEngine();

    public abstract IInterior getInterior();

    public abstract String getModel();

    public abstract void setModel(String model);

    public abstract String getBodyType();

    public abstract void setBodyType(String bodyType);

}
