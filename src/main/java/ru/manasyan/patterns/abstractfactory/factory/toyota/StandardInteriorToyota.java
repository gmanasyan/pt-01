package ru.manasyan.patterns.abstractfactory.factory.toyota;

import ru.manasyan.patterns.abstractfactory.factory.IInterior;

public class StandardInteriorToyota implements IInterior {

    private String name = "StandardInteriorToyota";

    @Override
    public String getName() {
        return name;
    }

}
