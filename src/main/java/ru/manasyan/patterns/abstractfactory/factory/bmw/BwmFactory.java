package ru.manasyan.patterns.abstractfactory.factory.bmw;

import ru.manasyan.patterns.abstractfactory.factory.CarFactory;
import ru.manasyan.patterns.abstractfactory.factory.IBody;
import ru.manasyan.patterns.abstractfactory.factory.IEngine;
import ru.manasyan.patterns.abstractfactory.factory.IInterior;

public class BwmFactory implements CarFactory {

    @Override
    public IBody createBody(String body) {
        switch (body) {
            case "Sedan" :  return new SedanBodyBmw();
            case "Hatchback" :  return new HatchbackBodyBmw();
        }
        return new SedanBodyBmw();
    }

    @Override
    public IEngine createEngine(String engine) {
        switch (engine) {
            case "Benzine" :  return new BenzineEngineBmw();
            case "Electro" :  return new ElectroEngineBmw();
            case "Diesel" :  return new DieselEngineBmw();
        }
        return new BenzineEngineBmw();
    }

    @Override
    public IInterior createInterior(String interior) {
        switch (interior) {
            case "Standard" :  return new StandardInteriorBmw();
            case "Premium" :  return new PremiumInteriorBmw();
        }
        return new StandardInteriorBmw();
    }


}
