package ru.manasyan.patterns.abstractfactory.factory.bmw;

import ru.manasyan.patterns.abstractfactory.factory.IEngine;

public class BenzineEngineBmw implements IEngine {

    private String name = "BenzineEngineBmw";

    @Override
    public String getName() {
        return name;
    }
}
