package ru.manasyan.patterns.abstractfactory.factory.toyota;

import ru.manasyan.patterns.abstractfactory.factory.IEngine;

public class DieselEngineToyota implements IEngine {

    private String name = "DieselEngineToyota";

    @Override
    public String getName() {
        return name;
    }
}
