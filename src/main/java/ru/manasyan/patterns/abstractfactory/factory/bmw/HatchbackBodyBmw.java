package ru.manasyan.patterns.abstractfactory.factory.bmw;

import ru.manasyan.patterns.abstractfactory.factory.IBody;

public class HatchbackBodyBmw implements IBody {

    private String name = "HatchbackBodyBmw";

    @Override
    public String getName() {
        return name;
    }
}
