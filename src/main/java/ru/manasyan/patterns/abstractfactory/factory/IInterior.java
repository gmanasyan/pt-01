package ru.manasyan.patterns.abstractfactory.factory;

public interface IInterior {
    String getName();
}
